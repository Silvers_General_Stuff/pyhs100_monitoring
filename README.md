This is my script to grab the data from my HS110 using the [pyHS100](https://github.com/GadgetReactor/pyHS100) libary.  
I have it set up on a cron that goes off once per minute.  
It dumps the data into InfluxDB
Then grafanda picks that up and graphs it.

And ye I know I should have a different passowrd than root/root but its on an internal network