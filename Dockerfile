FROM python:3

# install dependencies
COPY install.sh .
RUN ./install.sh

# copy the scripts into it
COPY powerPlugs.py .

CMD [ "python", "./powerPlugs.py" ]
