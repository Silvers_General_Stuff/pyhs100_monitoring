import os
from influxdb import InfluxDBClient
from pyHS100 import SmartPlug, Discover

data = []
for ip in Discover.discover():
	plug = SmartPlug(ip)
	tmp = {}
	tmp["measurement"] = "power"
	tmp["tags"] = {}
	tmp["tags"]["name"] = plug.get_sysinfo()["alias"]
	tmp["fields"] = plug.get_emeter_realtime()
	data.append(tmp)

print(data)
host = os.environ.get('DB_SERVER')
port = os.environ.get('DB_PORT')
client = InfluxDBClient(host=host,port=port, username='root', password='root', database='power', ssl=True, verify_ssl=True)
client.write_points(data)